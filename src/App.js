import React from "react";

// bọc tất cả các thành phần UI theo BrowserRouterđối tượng
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

//Information
//information


import AddInformation from "./components/AddInformation";
import Information from "./components/Information";
import InformationList from "./components/InformationsList";


/* app là bộ chứa gốc cho ứng dung của chúng ta, 
nó sẽ chứa một navbar bên trong <Router> ở trên 
và cũng là một Switchđối tượng có một số tệp Route
Mỗi Route điểm đến một Thành phần phản ứng
*/
function App() {
  return (
    <Router>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/informations" className="navbar-brand">
        Information
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/informations"} className="nav-link">
            Informations
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/add"} className="nav-link">
              Add
            </Link>
          </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={["/", "/informations"]} component={InformationList} />
          <Route exact path="/add" component={AddInformation} />
          <Route path="/informations/:id" component={Information} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;