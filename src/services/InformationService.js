/*
tạo một dịch vụ sử dụng đối tượng axios ở trên để gửi các yêu cầu HTTP.
xuất các hàm CRUD và phương thức tìm kiếm:

-TẠO NÊN:create
createInformation()
calls the InformationDataService.create()
dispatch CREATE_TUTORIAL

-LẤY LẠI: getAll,get
retrieveTutorials()
calls the TutorialDataService.getAll()
dispatch RETRIEVE_TUTORIALS

-CẬP NHẬT:update
updateTutorial()
calls the TutorialDataService.update()
dispatch UPDATE_TUTORIAL

-XÓA: remove,removeAll
deleteTutorial()
calls the TutorialDataService.remove()
dispatch DELETE_TUTORIAL

-TÌM KIẾM:findByName
findTutorialsByName()
calls the TutorialDataService.findByTitle()
 */
  //Information
//information
import http from "../http-common";

const getAll = () => {
  return http.get("/informations");
};

const get = id => {
  return http.get(`/informations/${id}`);
};

const create = data => {
  return http.post("/informations", data);
};

const update = (id, data) => {
  return http.put(`/informations/${id}`, data);
};

const remove = id => {
  return http.delete(`/informations/${id}`);
};

const removeAll = () => {
  return http.delete(`/informations`);
};

const findByName = name => {
  return http.get(`/informations?name=${name}`);
};

const InformationService = {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  findByName
};
/*
 * gọi các phương thức axios (được nhập dưới dạng http) 
 * get, post, put, deletetương ứng với các Yêu cầu 
 * HTTP: GET, POST, PUT, DELETE để thực hiện các Thao tác CRUD
 */
export default InformationService;
