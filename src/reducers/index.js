import { combineReducers } from "redux";
import informations from "./informations";

export default combineReducers({
  informations,
});
/**
 * Because we only have a single store in a Redux application. 
 * We use reducer composition instead of many stores to split data handling logic.
 * 
 * Bởi vì chúng tôi chỉ có một cửa hàng duy nhất trong ứng dụng Redux. 
 * Chúng tôi sử dụng thành phần bộ giảm tốc thay vì nhiều cửa hàng để phân chia logic xử lý dữ liệu.
 */