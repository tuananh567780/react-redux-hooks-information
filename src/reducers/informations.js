/**
 * Trình rút gọn hướng dẫn 
 * sẽ cập nhật(update) trạng thái hướng dẫn(tutorials) của cửa hàng(store) Redux:
 */
import {
    CREATE_INFORMATION,
    RETRIEVE_INFORMATIONS,
    UPDATE_INFORMATION,
    DELETE_INFORMATION,
    DELETE_ALL_INFORMATIONS,
  } from "../actions/types";

  const initialState = [];
  
  const informationReducer = (informations = initialState, action) => {
    const { type, payload } = action;
  
    switch (type) {
      case CREATE_INFORMATION:
        return [...informations, payload];
  
      case RETRIEVE_INFORMATIONS:
        return payload;
  
      case UPDATE_INFORMATION:
        return informations.map((information) => {
          if (information.id === payload.id) {
            return {
              ...information,
              ...payload,
            };
          } else {
            return information;
          }
        });
  
      case DELETE_INFORMATION:
        return informations.filter(({ id }) => id !== payload.id);
  
      case DELETE_ALL_INFORMATIONS:
        return [];
  
      default:
        return informations;
    }
  };
  
  export default informationReducer;