import axios from "axios";

/*
baseURLtùy thuộc vào url API REST mà Máy chủ của bạn định cấu hình
*/
export default axios.create({
  baseURL: "http://localhost:8090/api",
  headers: {
    "Content-type": "application/json"
  }
});