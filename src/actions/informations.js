import {
    CREATE_INFORMATION,
    RETRIEVE_INFORMATIONS,
    UPDATE_INFORMATION,
    DELETE_INFORMATION,
    DELETE_ALL_INFORMATIONS,
  } from "./types";
  


  import InformationDataService from "../services/InformationService";
  
  export const createInformation = (name, address, email, numphone) => async (dispatch) => {
    try {
      const res = await InformationDataService.create({ name, address, email, numphone });
  
      dispatch({
        type: CREATE_INFORMATION,
        payload: res.data,
      });
  
      return Promise.resolve(res.data);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  
  export const retrieveInformations = () => async (dispatch) => {
    try {
      const res = await InformationDataService.getAll();
  
      dispatch({
        type: RETRIEVE_INFORMATIONS,
        payload: res.data,
      });
    } catch (err) {
      console.log(err);
    }
  };
  
  export const updateInformation = (id, data) => async (dispatch) => {
    try {
      const res = await InformationDataService.update(id, data);
  
      dispatch({
        type: UPDATE_INFORMATION,
        payload: data,
      });
  
      return Promise.resolve(res.data);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  
  export const deleteInformation = (id) => async (dispatch) => {
    try {
      await InformationDataService.remove(id);
  
      dispatch({
        type: DELETE_INFORMATION,
        payload: { id },
      });
    } catch (err) {
      console.log(err);
    }
  };
  
  export const deleteAllInformations = () => async (dispatch) => {
    try {
      const res = await InformationDataService.removeAll();
  
      dispatch({
        type: DELETE_ALL_INFORMATIONS,
        payload: res.data,
      });
  
      return Promise.resolve(res.data);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  //
  export const findInformationsByName = (name) => async (dispatch) => {
    try {
      const res = await InformationDataService.findByName(name);
  
      dispatch({
        type: RETRIEVE_INFORMATIONS,
        payload: res.data,
      });
    } catch (err) {
      console.log(err);
    }
  };