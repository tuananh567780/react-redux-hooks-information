export const CREATE_INFORMATION = "CREATE_INFORMATION";
export const RETRIEVE_INFORMATIONS = "RETRIEVE_INFORMATIONS";
export const UPDATE_INFORMATION = "UPDATE_INFORMATION";
export const DELETE_INFORMATION = "DELETE_INFORMATION";
export const DELETE_ALL_INFORMATIONS = "DELETE_ALL_INFORMATIONS";