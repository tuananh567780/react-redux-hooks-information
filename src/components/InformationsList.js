  /*
   * Thành phần này có:
một thanh tìm kiếm để tìm Hướng dẫn theo tiêu đề.
một mảng hướng dẫn được hiển thị dưới dạng danh sách bên trái.
một Hướng dẫn đã chọn được hiển thị ở bên phải.
   */
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";


import {
  retrieveInformations,
  findInformationsByName,
  deleteAllInformations,
} from "../actions/informations";
import { Link } from "react-router-dom";

const InformationsList = () => {
  const [currentInformation, setCurrentInformation] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);
  const [searchName, setSearchName] = useState("");

  /**
   * Để kết nối Redux store với local Component state và props, 
   * chúng ta sử dụng useSelector() và useDispatch()
   */
  const informations = useSelector(state => state.informations);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(retrieveInformations());
  }, []);

  const onChangeSearchName = e => {
    const searchName = e.target.value;
    setSearchName(searchName);
  };

  const refreshData = () => {
    setCurrentInformation(null);
    setCurrentIndex(-1);
  };

  const setActiveInformation = (information, index) => {
    setCurrentInformation(information);
    setCurrentIndex(index);
  };

  const removeAllInformations = () => {
    dispatch(deleteAllInformations())
      .then(response => {
        console.log(response);
        refreshData();
      })
      .catch(e => {
        console.log(e);
      });
  };

  const findByName = () => {
    refreshData();
    dispatch(findInformationsByName(searchName));
  };

  return (
    <div className="list row">
      <div className="col-md-8">
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Search by name"
            value={searchName}
            onChange={onChangeSearchName}
          />
          <div className="input-group-append">
            <button
              className="btn btn-outline-secondary"
              type="button"
              onClick={findByName}
            >
              Search
            </button>
          </div>
        </div>
      </div>
      <div className="col-md-6">
        <h4>Information List</h4>

        <ul className="list-group">
          {informations &&
            informations.map((information, index) => (
              <li
                className={
                  "list-group-item " + (index === currentIndex ? "active" : "")
                }
                onClick={() => setActiveInformation(information, index)}
                key={index}
              >
                {information.name}
              </li>
            ))}
        </ul>
        
        <button
          className="m-3 btn btn-sm btn-danger"
          onClick={removeAllInformations}
        >
          Remove All
        </button>
      </div>
      <div className="col-md-6">
        {currentInformation ? (
          <div>
            <h4>Information</h4>
            <div>
              <label>
                <strong>name:</strong>
              </label>{" "}
              {currentInformation.name}
            </div>
            <div>
              <label>
                <strong>Address:</strong>
              </label>{" "}
              {currentInformation.address}
            </div>
            <div>
              <label>
                <strong>email:</strong>
              </label>{" "}
              {currentInformation.email}
            </div>
            <div>
              <label>
                <strong>numphone:</strong>
              </label>{" "}
              {currentInformation.numphone}
            </div>
            <div>
              <label>
                <strong>Status:</strong>
              </label>{" "}
              {currentInformation.published ? "Published" : "Pending"}
            </div>

            <Link
              to={"/informations/" + currentInformation.id}
              className="badge badge-warning"
            >
              Edit
            </Link>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Information...</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default InformationsList;