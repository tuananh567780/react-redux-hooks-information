/**
 * Thành phần này sẽ sử dụng phương thức TutorialDataService.get() 
 * trong Effect Hook useEffect() để nhận Hướng dẫn theo id trong URL.

Để cập nhật, xóa Hướng dẫn
updateTutorial
deleteTutorial

 */

//Information
//information

import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { updateInformation, deleteInformation } from "../actions/informations";
import InformationDataService from "../services/InformationService";

const Information = (props) => {
  const initialInformationState = {
    id: null,
    name: "",
    address: "",
    email: "",
    numphone: "",
    description: "",
    published: false
  };
  const [currentInformation, setCurrentInformation] = useState(initialInformationState);
  const [message, setMessage] = useState("");

  const dispatch = useDispatch();

  const getInformation = id => {
    InformationDataService.get(id)
      .then(response => {
        setCurrentInformation(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  useEffect(() => {
    getInformation(props.match.params.id);
  }, [props.match.params.id]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentInformation({ ...currentInformation, [name]: value });
  };

  const updateStatus = status => {
    const data = {
      id: currentInformation.id,
      name: currentInformation.name,
      address: currentInformation.address,
      email: currentInformation.email,
      numphone: currentInformation.numphone,
      published: status
    };

    dispatch(updateInformation(currentInformation.id, data))
      .then(response => {
        console.log(response);

        setCurrentInformation({ ...currentInformation, published: status });
        setMessage("The status was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const updateContent = () => {
    dispatch(updateInformation(currentInformation.id, currentInformation))
      .then(response => {
        console.log(response);

        setMessage("The tutorial was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const removeInformation = () => {
    dispatch(deleteInformation(currentInformation.id))
      .then(() => {
        props.history.push("/informations");
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <div>
      {currentInformation ? (
        <div className="edit-form">
          <h4>Information</h4>
          <form>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={currentInformation.name}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="address">Address</label>
              <input
                type="text"
                className="form-control"
                id="address"
                name="address"
                value={currentInformation.address}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="text"
              className="form-control"
              id="email"
              required
              value={currentInformation.email}
              onChange={handleInputChange}
              name="email"
            />
          </div>
          <div className="form-group">
            <label htmlFor="numphone">Numphone</label>
            <input
              type="text"
              className="form-control"
              id="numphone"
              required
              value={currentInformation.numphone}
              onChange={handleInputChange}
              name="numphone"
            />
          </div>

            <div className="form-group">
              <label>
                <strong>Status:</strong>
              </label>
              {currentInformation.published ? "Published" : "Pending"}
            </div>
          </form>

          {currentInformation.published ? (
            <button
              className="badge badge-primary mr-2"
              onClick={() => updateStatus(false)}
            >
              UnPublish
            </button>
          ) : (
            <button
              className="badge badge-primary mr-2"
              onClick={() => updateStatus(true)}
            >
              Publish
            </button>
          )}

          <button className="badge badge-danger mr-2" onClick={removeInformation}>
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={updateContent}
          >
            Update
          </button>
          <p>{message}</p>
        </div>
      ) : (
        <div>
          <br />
          <p>Please click on a Information...</p>
        </div>
      )}
    </div>
  );
};

export default Information;