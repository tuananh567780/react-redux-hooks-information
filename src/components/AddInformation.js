import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { createInformation } from "../actions/informations";
//Information
//information

const AddInformation = () => {
  const initialInformationState = {
    id: null,
    name: "",
    address: "",
    email: "",
    numphone: "",
    published: false
  };

  //xác định và đặt trạng thái ban đầu:
  const [information, setInformation] = useState(initialInformationState);
  const [submitted, setSubmitted] = useState(false);

  const dispatch = useDispatch();

  //Tạo handleInputChange()chức năng theo dõi các giá trị của đầu vào 
  //và đặt trạng thái đó cho các thay đổi.
  const handleInputChange = event => {
    const { name, value } = event.target;
    setInformation({ ...information, [name]: value });
  };

  /**
   * có chức năng nhận trạng thái hướng dẫn cục bộ và gửi yêu cầu POST tới API Web. 
   * Nó gửi hành động với trình tạo hành động createTutorial() với useDispatch().
   * Móc này trả về một tham chiếu đến hàm điều phối từ cửa hàng Redux.
   */
  const saveInformation = () => {
    const { name, address, email, numphone } = information;

    dispatch(createInformation(name, address, email, numphone))
      .then(data => {
        setInformation({
          id: data.id,
          name: data.name,
          address: data.address,
          email: data.email,
          numphone: data.numphone,
          published: data.published
        });
        setSubmitted(true);

        console.log(data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const newInformation = () => {
    setInformation(initialInformationState);
    setSubmitted(false);
  };


  /**
   * Để trả lại, kiểm tra trạng thái đã gửi, nếu đúng, 
   * sẽ hiển thị lại nút Thêm để tạo Hướng dẫn mới. 
   * Nếu không, một Biểu mẫu có nút Gửi sẽ hiển thị.
   */
  return (
    <div className="submit-form">
      {submitted ? (
        <div>
          <h4>You submitted successfully!</h4>
          <button className="btn btn-success" onClick={newInformation}>
            Add
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              required
              value={information.name}
              onChange={handleInputChange}
              name="name"
            />
          </div>

          <div className="form-group">
            <label htmlFor="address">Address</label>
            <input
              type="text"
              className="form-control"
              id="address"
              required
              value={information.address}
              onChange={handleInputChange}
              name="address"
            />
          </div>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="text"
              className="form-control"
              id="email"
              required
              value={information.email}
              onChange={handleInputChange}
              name="email"
            />
          </div>
          <div className="form-group">
            <label htmlFor="numphone">Numphone</label>
            <input
              type="text"
              className="form-control"
              id="numphone"
              required
              value={information.numphone}
              onChange={handleInputChange}
              name="numphone"
            />
          </div>
  
          <button onClick={saveInformation} className="btn btn-success">
            Submit
          </button>
        </div>
      )}
    </div>
  );
};

export default AddInformation;